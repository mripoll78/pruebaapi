﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PruebaSD.Models;
using PruebaSD.Clases;

namespace PruebaSD.Controllers
{
    [RoutePrefix("api/users")]
    public class UsuariosController : ApiController
    {
        private PruebaSDEntities db = new PruebaSDEntities();

        public struct response
        {
            public string type;
            public string message;
            public Object data;
            public response(string Type, string Message, Object Data)
            {
                type = Type;
                message = Message;
                data = Data;
            }
        }

        [HttpGet]
        [Route("getUsers")]
        public IHttpActionResult getUsers()
        {
            try
            {
                List<Usuarios> u = db.Usuarios.ToList();
                return Ok(u);
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        [Route("postUsers")]
        public IHttpActionResult postUsers(Usuario _u)
        {
            response r = new response();
            r.type = "error";

            try
            {
                if (_u == null)
                {
                    r.message = "Objeto null";
                    r.data = null;

                    return Ok(r);
                }

                Usuarios u_ = new Usuarios();

                u_.nombre = _u.nombre;
                u_.apellido = _u.apellido;

                db.Usuarios.Add(u_);

                try
                {
                    db.SaveChanges();

                    r.type = "success";
                    r.message = "Guardado correctamente";
                    r.data = u_;

                    return Ok(r);
                }
                catch (Exception ex)
                {
                    r.message = "Exception";
                    r.data = ex;

                    return Ok(r);
                }
            }
            catch (Exception ex)
            {
                r.message = "Exception";
                r.data = ex;

                return Ok(r);
            }
        }

        [HttpPost]
        [Route("deleteUsers")]
        public IHttpActionResult deleteUsers(int id)
        {
            response r = new response();
            r.type = "error";

            try
            {
                Usuarios u = db.Usuarios.Where(x => x.usuId == id).FirstOrDefault();

                if (u == null)
                {
                    r.message = "Usuario no existe";
                    r.data = null;

                    return Ok(r);
                }

                db.Usuarios.Remove(u);

                try
                {
                    db.SaveChanges();

                    r.type = "success";
                    r.message = "Usuario eliminado";
                    r.data = u;

                    return Ok(r);
                }
                catch (Exception ex)
                {
                    r.message = "Exception";
                    r.data = ex;

                    return Ok(r);
                }
            }
            catch (Exception ex)
            {
                r.message = "Exception";
                r.data = ex;

                return Ok(r);
            }
        }

        [HttpGet]
        [Route("getByIdUsers")]
        public IHttpActionResult getByIdUsers(int id)
        {
            try
            {
                Usuarios u = db.Usuarios.Where(x => x.usuId == id).FirstOrDefault();

                return Ok(u);
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        [Route("putUsers")]
        public IHttpActionResult putUsers(Usuario u, int id)
        {
            response r = new response();
            r.type = "error";

            try
            {
                Usuarios _u = db.Usuarios.Where(x => x.usuId == id).FirstOrDefault();
                
                if (u == null)
                {
                    r.message = "Usuario no existe";
                    r.data = null;

                    return Ok(r);
                }

                _u.nombre = u.nombre;
                _u.apellido = u.apellido;

                db.Entry(_u).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();

                    r.type = "success";
                    r.message = "Usuario Actualizado";
                    r.data = u;

                    return Ok(r);
                }
                catch (Exception ex)
                {
                    r.message = "Exception";
                    r.data = ex;

                    return Ok(r);
                }
            }
            catch (Exception ex)
            {
                r.message = "Exception";
                r.data = ex;

                return Ok(r);
            }
        }

    }
}
